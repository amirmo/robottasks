*** Settings ***
Library    SeleniumLibrary
Library     DataDriver    ../TestData/ExcelData.xlsx      sheet_name=Sheet1
Resource    ..//Resources//commons.robot
Resource    ..//Resources//Locators.robot

Test Template   Fill Form
Test Setup    open browser    ${URL}   ${Browser}
...     maximize browser window
Test Teardown    close browser


*** Test Cases ***
Facebook Log In with ${userEmail} and ${userPass}


*** Keywords ***
Fill Form
    [Arguments]    ${userEmail}     ${userPass}
    input text    ${EmailField}      ${userEmail}
    input text    ${PasswordField}       ${userPass}
    click button    ${LogInBtn}
    page should contain    Find Friends
    sleep    5s




